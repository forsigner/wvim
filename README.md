## 用法

1. 进入用户文件夹：<code>cd c:\Users\<username></code>
2. 下载，<code>git clone git@github.com:forsigner/wvim.git</code>
3. 执行下面命令：

``` js
mklink _vimrc C:\Users\<username>\wvim\vimrc
mklink /D vimfiles C:\Users\<username>\wvim\vim
```

ps: 

username代表用户文件夹
