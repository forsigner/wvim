" ==========================================================="
" 基础配置
" ==========================================================="
"
" 设置通用缩进策略
set shiftwidth=4
set softtabstop=4           " insert mode tab and backspace use 2 spaces
set tabstop=4

" 对部分语言设置单独的缩进
" au FileType js set shiftwidth=2
" au FileType js set tabstop=2

set backspace=2              " 设置退格键可用
set synmaxcol=2048           " Syntax coloring too-long lines is slow
set clipboard=unnamed        " yank and paste with the system clipboard
set directory-=.             " don't store swapfiles in the current directory
set autoindent               " 自动对齐
set backupcopy=yes           " see :help crontab
set ai!                      " 设置自动缩进
set smarttab
set smartindent              " 智能自动缩进
set number                   " 显示行号
set ruler                    " 右下角显示光标位置的状态行
set incsearch                " 开启实时搜索功能
set hlsearch                 " 开启高亮显示结果
set nowrapscan               " 搜索到文件两端时不重新搜索
set nocompatible             " 关闭兼容模式
set hidden                   " 允许在有未保存的修改时切换缓冲区
set autochdir                " 设定文件浏览器目录为当前目录
set foldmethod=indent        " 选择代码折叠类型
set foldlevel=100            " 禁止自动折叠
set laststatus=2             " 开启状态栏信息
set cmdheight=1              " 命令行的高度，默认为1，这里设为2
set autoread                 " 当文件在外部被修改时自动更新该文件
set nobackup                 " 不生成备份文件
set nowritebackup
set noswapfile               " 不生成交换文件
set list                     " 显示特殊字符，其中Tab使用高亮~代替，尾部空白使用高亮点号代替
set listchars=tab:\~\ ,trail:-
" set listchars=tab:▸\ ,trail:▫
"set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace
"set listchars=tab:>-,trail:-
set expandtab                " 将Tab自动转化成空格 [需要输入真正的Tab键时，使用 Ctrl+V + Tab]
"set showmatch               " 显示括号配对情况
"set nowrap                  " 设置不自动换行
"

set wildmenu                 " show a navigable menu for tab completion
set wildignore=log/**,node_modules/**,target/**,tmp/**,*.rbc
set wildmode=longest,list,full
set nocursorline " don't highlight current line

syntax enable                " 打开语法高亮
syntax on                    " 开启文件类型侦测
filetype indent on           " 针对不同的文件类型采用不同的缩进格式
filetype plugin on           " 针对不同的文件类型加载对应的插件
filetype plugin indent on    " 启用自动补全
"

" 设置leader"
let maplocalleader = ";"
let mapleader= ";"


" ==========================================================="
" 快捷键
" ==========================================================="
"
"窗口跳转
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" tab操作
map tt :tabnew<cr>
map tn :tabnext<cr>
map tp :tabprevious<cr>

" 跳转到特定的tab
map t1 :tabfirst<CR>
map t2 :tabm 1<CR>
map t3 :tabm 2<CR>
map t4 :tabm 3<CR>
map t5 :tabm 4<CR>
map t6 :tabm 1<CR>
map t7 :tabm 1<CR>
map t8 :tabm 2<CR>
map t9 :tabm 3<CR>
map t0 :tablast<CR>

map te :tabedit
map tc :tabclose<cr>

" 分割窗口
map ws :split<cr>
map wv :vsplit<cr>

" 关闭窗口
map wc :close<cr>

" 切换分割窗口
map wn <C-w>w

" 跳转到屏幕显示的下一行或上一行，而不是真实的上一行或下一行
noremap j gj
noremap k gk

" 将所选中内容复制到系统
map cy "+y

" 模仿MS Windows中的快捷键
vmap <C-c> "+y
vmap <C-x> "yd
nmap <C-v> "+p
vmap <C-v> "+p
nmap <C-a> ggvG$

" 模仿MS Windows中的保存命令: Ctrl+S
imap <C-s> <Esc>:wa<cr>i<Right>
nmap <C-s> :wa<cr>

" Easier horizontal scrolling
map zl zL
map zh zH

" replace(替换)
nnoremap <Leader>s :%s//g<left><left>
vnoremap <Leader>s :s//g<left><left>

" 把tab转换成空格
nnoremap <Leader>t :retab<cr>
vnoremap <Leader>t :retab<cr>

" 格式化代码
nmap <Leader>g gg=G
vmap <Leader>g gg=G
imap <Leader>g gg=G


" 快速关闭窗口
nnoremap <leader>q :q!<CR>


"快速打开 _vimrc
map <silent><leader>1 :e ~/_vimrc<cr>
map <silent><leader>2 :e ~/_vimrc.bundles<cr>
map <silent><leader>3 :e ~/_vimrc.plugin.settings<cr>

" 修改nginx配置文件
map <silent><leader>4 :e ~/nginx-1.9.1/conf/nginx.conf<cr>


" 打开hosts文件
map <silent><leader>5 :e C:\Windows\System32\drivers\etc\hosts<cr>


map <silent> <leader>V :source ~/_vimrc<CR>:filetype detect<CR>:exe ":echo 'vimrc reloaded'"<CR>

" 快速ESC
imap <Leader>e <ESC>
nmap <Leader>e <ESC>
vmap <Leader>e <ESC>
cmap <Leader>e <ESC>


nnoremap <Leader>` :cd ~/vhost/<right>
vnoremap <Leader>` :cd ~/vhost/<right>

" ==========================================================="
" 自动执行命令
" ==========================================================="
"自动去trailing scpace
autocmd BufWritePost *.js,*.html,*.css,*.scss :%s/\s\+$//e

" autocmd BufWritePost,BufRead,BufNewFile *.scss setlocal filetype=scss
" autocmd BufNewFile,BufRead *.scss set filetype=scss
" au BufRead,BufNewFile *.scss set filetype=scss



" ==========================================================="
" 编码格式
" ==========================================================="

" 设置文件编码和文件格式
set fenc=utf-8
set encoding=utf-8
set fileencodings=utf-8,gbk,cp936,latin-1
set fileformat=unix
set fileformats=unix,mac,dos
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim
language messages zh_CN.utf-8


" ==========================================================="
" 界面配置
" ==========================================================="
" 使用GUI界面时的设置
if has("gui_running")

    " 启动时自动最大化窗口
    au GUIEnter * simalt ~x
    "winpos 20 20            " 指定窗口出现的位置，坐标原点在屏幕左上角
    "set lines=20 columns=90 " 指定窗口大小，lines为高度，columns为宽度
    "
    set guioptions+=c        " 使用字符提示框
    set guioptions-=m        " 隐藏菜单栏
    set guioptions-=T        " 隐藏工具栏
    set guioptions-=L        " 隐藏左侧滚动条
    set guioptions-=r        " 隐藏右侧滚动条
    set guioptions-=b        " 隐藏底部滚动条
    " set showtabline=0        " 隐藏Tab栏
    "set cursorline           " 高亮突出当前行
    "set cursorcolumn        " 高亮突出当前列
    "
    " 设置着色模式和字体
    colorscheme molokai
    set guifont=Monaco:h11
endif

"======================================================
" 插件管理
"======================================================

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" install Vundle bundles
if filereadable(expand("~/_vimrc.bundles"))
  source ~/_vimrc.bundles
endif

call vundle#end()            " required
filetype plugin indent on    " required

"======================================================
" 插件设置
"======================================================
if filereadable(expand("~/_vimrc.plugin.settings"))
  source ~/_vimrc.plugin.settings
endif
